This module to add a sliding panel feature

Sliding_Popup uses a combination of JavaScript, JQuery and CSS3 to implement the
feature of a popup panel on the screen.  This implements a sliding panel from left 
or right.  the jQuery calculates the height of the trigger so the panel will have it's top
just above the trigger. The jQuery also calculates the height of the panel to see if it
will go past the logical end of page.  jQuery will change the height of the panel and
set the panel to have vertical scrolling. 

The minimum required files for a module are slider_popup.admin.inc,
slider_popup.info and slider_popup.module.  I added a requirement in
the slider_popup.info for ckeditor, so that the text_format field can be WYSISYG.

Author: Susan Fox (sfox2681 at gmail dot com)
Website: http://fox-custom-software.com


