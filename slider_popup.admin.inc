<?php

/**
 * @file
 * Administrative page callbacks for the slider_popup module.
 */

/**
 * Configuration form for Slider Popup
 */
function slider_popup_config($form, &$form_state) {

  $settings = array();
  $settings = adminslider_get_settings();


  // fieldgroups
  $form['trigger'] = array(
    '#type' => 'fieldset',
    '#title' => t('Trigger Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['panel-settings'] = array (
    '#type' => 'fieldset',
    '#title' => t('Popup Panel Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // trigger elements
  $form['trigger']['button-desc'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Description'),
    '#default_value' => $settings['button-desc'],
    '#description' => t('This is the name to appear on the Slider Button'),
  );

  $form['panel-settings']['configuration'] = array (
      '#type' => 'fieldset',
      '#title' => t('Panel Settings'),
      '#weight' => 1,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
   );

  // panel elements
   $form['panel-settings']['configuration']['popup-panel-div'] = array (
    '#type' => 'textfield',
    '#title' => t('Name of Popup panel Div'),
    '#default_value' => $settings['popup-panel-div'],
    '#description' => t('This field gives you the ability to name the div of the panel.'),

  );
  $form['panel-settings']['configuration']['popup-panel-loc'] = array (
    '#type' => 'select',
    '#title' => t('Popup panel Direction'),
    '#options' => array(
      0 => t('left'),
      1 => t('right'), /**
      2 => t('top'),
      3 => t('bottom'), **/
     ),
    '#default_value' => $settings['popup-panel-loc'],

    '#description' => t('Set this so the popup panel slides from left or right.'),

  );
  $form['panel-settings']['configuration']['popup-panel-use'] = array (
    '#type' => 'select',
    '#title' => t('Use Panel defined here'),
    '#options' => array(
      0 => t('Yes'),
      1 => t('No'),
     ),
    '#default_value' => $settings['popup-panel-use'],

    '#description' => t('Set this to Yes to use the admin defined panel or No to define your own block with a class of panel.'),

  );
  $form['panel-settings']['configuration']['slider-popup-panel'] = array (
    '#type' => 'text_format',
    '#title' => t('Text to appear in Popup'),
    '#rows' => 15,
    '#default_value' => $settings['slider-popup-panel'],
    '#format' => $settings['slider-panel-format'],
    '#description' => t('Set this to the information you want to appear in the Slider Popup'),

  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler of configuration form.
 */
function slider_popup_config_submit($form, &$form_state) {
  $settings = array();
  $settings['button-desc'] = $form_state['values']['button-desc'];
  $settings['popup-panel-div'] = $form_state['values']['popup-panel-div'];
  $settings['popup-panel-loc'] = $form_state['values']['popup-panel-loc'];
  $settings['popup-panel-use'] = $form_state['values']['popup-panel-use'];
  // ckeditor puts the panel text into an array of 'value' and 'format'
  $popupPanel = array();
  $popupPanel = $form_state['values']['slider-popup-panel'];

  $settings['slider-popup-panel'] = $popupPanel['value'];
  $settings['slider-panel-format'] = $popupPanel['format'];
  //dpm($settings);


  variable_set('slider_popup_settings', $settings);
  //variable_set('slider_popup_buttons', $buttons);
  drupal_set_message (t('Configuration changes have been saved'));
  //drupal_set_message($buttons[0]['popup-outer-button']);
}

function adminslider_get_settings() {
  $settings = array();

  $settings = variable_get('slider_popup_settings');

  if (count($settings) == 0) {

      $settings['button-desc'] = "Testing Button";
      $settings['popup-panel-div'] = "myPanel";
      $settings['slider-popup-panel'] =  "";
      $settings['popup-panel-loc'] = 0;
      $settings['popup-panel-use'] = 0;
      $settings['slider-panel-format'] = "filtered_html";
  }


   return $settings;
}


function slider_get_buttons() {
  $buttons = array();
  variable_del('slider_popup_buttons');
  $buttons = variable_get('slider_popup_buttons',array());


  if (count($buttons,1) == 0) {
    for ($i = 0; $i < 4; $i++) {
          $buttons[$i]['popup-outer-button'] = "Empty Outer Button";
          $buttons[$i]['popup-inner-button'] = "Inner Button";
          $buttons[$i]['popup-href-button'] = "#";
    }
    return $buttons;

  }
}



function _slider_button_form($button_data) {

$button_form = array();

$button_form['popup-outer-button'] = array(
    '#type' => 'textfield',
    '#title' => t('Outer Button Description'),
    '#default_value' => $button_data['popup-outer-button'],
    '#description' => t('This is the text that will appear in the initial or Outer view of Button'),
  );

  $button_form['popup-href-button'] = array(
    '#type' => 'textfield',
    '#title' => t('HTML HREF Button Destination'),
    '#default_value' => $button_data['popup-href-button'],
    '#description' => t('This is the HREF destination for this button'),
  );

  $button_form['popup-inner-button'] = array(
    '#type' => 'textfield',
    '#title' => t('Inner Button Description'),
    '#default_value' => $button_data['popup-inner-button'],
    '#description' => t('This is the text that will appear in the sliding Inner view of Button'),
  );

  return $button_form;
}
