// jQuery .toggle("fast") causes the .panel element to be visible/invisible
// jQuery .toggleClass("active") causes one or more class names to be toggled
// for each element in the matched set

(function ($) {
	$(document).ready(function(){
	  
    $(".trigger").click(function(){
          // Calculate the offset of the main-wrapper and footer-wrapper to
          // see if the panel will go below the top of the footer or even
          // beyond the bottom of the footer
          // truncate the size of the panel to difference of main-wrapper.top 
          // and footer-wrapper.top 
          // and set overflow-y to scroll
          
          var mainOffset = $("#main-wrapper").offset();
          var footOffset = $("#footer-wrapper").offset();          
          var panHeight =  $(".panel").height();
          
          var panBottom = mainOffset.top + panHeight;
          
          if (panBottom > footOffset.top) {
            var newpanHeight = footOffset.top - mainOffset.top;
            $(".panel").css("height",newpanHeight);
            $(".panel").css("overflow-y","scroll");
          }
            
          var bHeight = $(".trigger").height(); // get height of trigger
          var bWidth = $(".trigger-div").width();
          var tWidth = $(".trigger").width();
          var extraSpace = tWidth - bWidth; // trigger is < trigger-div
          var marginRt = - (extraSpace + 40);
         
         // Calculate the panel top to be height of trigger + 10 (do a negative top)
         
          var pTop = -(bHeight + 10);
          var pPadLeft = bWidth; 
          var pPadRight = bWidth;
          var pPadDefault = $('.panel').css("padding-top");
          
          var flyFromDirection = $(".panel-orient").text(); 
                
          $(".panel").css("top", pTop);
          
          if (flyFromDirection == 'left') {
              $(".panel").css("padding-left",pPadLeft); 
              $(".panel").css("padding-right",pPadDefault);
              $(".panel").css("margin-left",20);
            }
          else {
              $(".panel").css("padding-right",pPadRight); 
              $(".panel").css("padding-left",pPadDefault);
              $(".panel").css("margin-right",marginRt);
              
          }
                    
          $(".panel").toggle("fast");
	        $(this).toggleClass("active");
	        return false;
	    });
	});
})(jQuery);
